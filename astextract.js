'use strict';
const fs = require('fs');
const jp = require('jspath');

let filePath = process.argv[2];
let rawdata = fs.readFileSync(0, 'utf-8');
let ast = JSON.parse(rawdata);

const getParams = funcDef =>
    jp.apply('.args.args{.ast_type==="arg"}', funcDef)
    .filter(item => item.arg != 'self')
    .map(item => item.arg);

const getFunctions = classDef => 
    jp.apply('.body{.ast_type==="FunctionDef"}', classDef)
    .filter(item => !['__init__', 'cast'].includes(item.name))
    .map(funcDef => {
        let result = {
            type: getFuncType(funcDef),
            name: funcDef.name, 
            returnType: getFuncReturnType(funcDef),
            params: getParams(funcDef)
        };
        let comment = jp.apply('.body{.ast_type==="Expr"}.value{.ast_type==="Constant"}.value', funcDef);
        if (comment.length) {
            result.comment = comment[0].trim();
        }    
        return result;
    });

const getClasses = ast => 
    jp.apply('.body{.ast_type==="ClassDef"}', ast)
    .map(classDef => {
        let result = {
            name: classDef.name, 
            functions: getFunctions(classDef)
        };    
        let comment = jp.apply('.body{.ast_type==="Expr"}.value{.ast_type==="Constant"}.value', classDef);
        if (comment.length) {
            result.comment = comment[0].trim();
        }    
        return result;
    });    

let ast2 = getClasses(ast);
console.log(JSON.stringify(ast2, null, '  '));
/*
{classes: [
    {
        name: "aclassnName",
        parent: "aParentClassName"  //FIXME
        comment: "sClassComment",
        functions: [
            {
                name: "aFuncName",
                comment: "aFuncComment",
                type: "property, setter, function, staticFunction"
                params: [
                    {
                        name: "anParamName",
                        comment: "aParamComment"
                    },
                    ...
                ]
                returnType: "aType"
                //FIXME return tuples
            },
            ...
        ]
    },
    ...
]}
*/

function getFuncType(funcDef) {
    if (funcIsProperty(funcDef)) {
        return "property"
    } else if (funcIsSetter(funcDef)) {
        return "setter"
    } else if (funcIsStaticMethod(funcDef)) {
        return "staticMethod"
    } else {
        return "function";
    }
}

function funcIsProperty(funcDef) {
    return jp.apply('.decorator_list{.ast_type==="Name" && .id==="property"}', funcDef).length > 0;
}

function funcIsStaticMethod(funcDef) {
    return jp.apply('.decorator_list{.ast_type==="Name" && .id==="staticmethod"}', funcDef).length > 0;
}

function funcIsSetter(funcDef) {
    return jp.apply('.decorator_list{.ast_type==="Attribute" && .attr==="setter"}', funcDef).length > 0;
}

function getFuncReturnType(funcDef) {
    let rt = jp.apply('.body{.ast_type==="Return"}.value{.ast_type==="Call"}.func{.ast_type==="Name"}.id', funcDef);
    return rt.length > 0 ? rt[0] : null;
}