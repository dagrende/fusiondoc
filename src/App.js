import logo from './logo.svg';
import './App.css';
import ast2 from './ast/drawing.json'

function App() {
  return (
    <div className="App">
      <ul>
        {ast2.map(item => 
          <li>{item.name}</li>
        )}
      </ul>
    </div>
  );
}

export default App;
